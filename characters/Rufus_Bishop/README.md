# Diff-able JSON
```bash
$ jq -S . Praulodan_Fuxfell.json | sponge Praulodan_Fuxfell.json
```

## Kampfsonderfertigkeiten
- [Meisterparade](https://ulisses-regelwiki.de/KSF_Meisterparade.html)
- [Feindgespür](https://www.ulisses-regelwiki.de/KSF_Feindgesp%C3%BCr.html)

## Liturgien
| Name                                                                            | Beschreibung                                                       | AP  |
|:--------------------------------------------------------------------------------|:-------------------------------------------------------------------|:----|
| [Hauch des Elements](https://de.wiki-aventurica.de/wiki/Hauch_des_Elements)     | Elemente (Wasser, Feuer, Erde, Luft) kann als Waffe benutzt werden |     |
| [Doppelgänger](https://de.wiki-aventurica.de/wiki/Doppelg%C3%A4nger_(Liturgie)) | Erzeugt Doppelgänger (zur ablenkung)                               |     |
| [Auge des Händlers](https://de.wiki-aventurica.de/wiki/Auge_des_H%C3%A4ndlers)  | Erfährt Anzahl von Dingen oder Personen in einem Raum              |     |
| [Wieselflink](https://de.wiki-aventurica.de/wiki/Wieselflink)                   | GE++                                                               |     |
| [Liturgieschild](https://de.wiki-aventurica.de/wiki/Liturgieschild)             | Schützt gegen offensiv Lit. anderer Geweihter                      |     |
| [Geisterfalle](https://de.wiki-aventurica.de/wiki/Geisterfalle)                 | Bindet einen Geist an einen Gegenstand                             |     |
| [Krigesfarben](https://de.wiki-aventurica.de/wiki/Kriegsfarben)                 | Kann durch bemalung die Stärke eines Tieres herbeirufen            |     |
| [Geldwechsel](https://de.wiki-aventurica.de/wiki/Geldwechsel)                   | Währung wechsel (Bsp: Dukaten <-> Dinar)                           |     |
