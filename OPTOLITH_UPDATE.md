# How to update Optolith
```
OPTOLITH_VERSION="$(\
    ls optolith/ \
    | grep --color=never -oE '[0-9]+(\.[0-9]+){2}' \
    | sort -V \
    | tail -1 \
)" \
&& docker build \
    --rm \
    --cache-from=opto \
    --build-arg OPTOLITH_VERSION="$OPTOLITH_VERSION" \
    -t opto \
    . \
&& docker run \
    -it \
    -e DEBUG=true \
    -e LANGUAGE=de \
    -v $PWD:/repo \
    -p 5900:5900 \
    -p 6080:6080 \
    -t opto "cd /repo; bash start_conversion.sh --debug; bash"
```

Start VNC client via [debug.vnc](debug.vnc).

In the container shell:

```bash
python -i ./optolith2pdf.py \
  --file characters/Rufus_Bishop/Rufus_Bishop.json \
  --images recognition/images/ \
  --numbers \
  --language de \
  --log_level DEBUG
```

> **NOTE:** If one has trouble to gain access to the exposed ports, it might help to do a ```systemctl restart docker```.

To get mouse positions or so, use: `xdotool getmouselocation --shell`.

# Development cycle
If you want to run the python script right away you can use the following command:
```bash
OPTOLITH_VERSION="$(\
    ls optolith/ \
    | grep --color=never -oE '[0-9]+(\.[0-9]+){2}' \
    | sort -V \
    | tail -1 \
)" \
&& docker build \
    --rm \
    --cache-from=opto \
    --build-arg OPTOLITH_VERSION="$OPTOLITH_VERSION" \
    -t opto \
    . \
&& docker run \
    -it \
    -e DEBUG=true \
    -e LANGUAGE=de \
    -v $PWD:/repo \
    -p 5900:5900 \
    -p 6080:6080 \
    -t opto "
        cd /repo \
        && bash start_conversion.sh --debug \
        && python -i ./optolith2pdf.py \
          --file characters/Rufus_Bishop/Rufus_Bishop.json \
          --images recognition/images/ \
          --numbers \
          --language de \
          --log_level DEBUG \
      "
```
