<img src="./docs/dsa_thumbnail.png" height="350px">

# DSA Charakter Sammlung

## Struktur
In der Regel gibt es pro Spieler ein Unterordner (mit Namen des Spielers) in [`./charaters`](./characters).

## Techstack
Es sollte mindestens die [Optolith-Heldenverwaltung] JSON-Datei in dem
einem Unterordner von [`./charaters`](./characters) abgelegt werden.
Alles übrige ist dem Spieler überlassen.

Zu jeder Optolith JSON Datei die in [`./charaters/**`](./characters) zu
finden ist, wird eine PDF generiert. Nach ein paar Minuten sind alle
[hier](https://milkpirate.gitlab.io/dsa-characters/) zu finden.

Die Charakterbögen von Branches liegen unter
`https://milkpirate.gitlab.io/dsa-characters/branches/<branch>` bereit.

## ACHTUNG!
Zur Zeit wird Optolith in Version 1.5.0 verwendet. Wird ein Heldenbogen
mit einer neueren Version bearbeitet/erstellt, kann es sein, dass die CI
bricht und keine PDFs erstellt werden. Dann wären anpassungen im
[converter script](./optolith2pdf.py) nötig.

## Nützliche Links
- [DSA Wiki] - Nützlich für die schnelle Suche von bspw. Sonderfertigkeiten,
Waffen, Regeln usw.
- [Optolith-Heldenverwaltung] - Zur Erstellung und Verwaltung von
Heldenbögen.
- [Fantasy Cities] - Karten-Generierung für Städte, Dörfer usw.
- [Aves Pfade] - Routernplanung in und Karte von Aventurien.


[DSA Wiki]: https://ulisses-regelwiki.de/index.php
[Aves Pfade]: http://www.avespfade.de/
[Fantasy Cities]: http://fantasycities.watabou.ru/
[Optolith-Heldenverwaltung]: https://www.ulisses-ebooks.de/product/209711/Optolith-Heldenverwaltung&language=de
[Pipelines]: https://gitlab.com/milkpirate/dsa-characters/-/pipelines
