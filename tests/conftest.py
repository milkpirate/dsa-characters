#!/usr/bin/env python3

from unittest import mock
import pathlib
import sys


for path in '.', '..':
    module_path = pathlib.Path(path)
    module_path = module_path.absolute().__str__()
    sys.path.append(module_path)

for module_to_mock in 'pyautogui', 'numpy', 'pyscreeze':
    sys.modules[module_to_mock] = mock.MagicMock()
