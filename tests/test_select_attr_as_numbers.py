#!/usr/bin/env python3

import pytest
from unittest import mock

import optolith2pdf

optolith2pdf.logger = mock.MagicMock()


@pytest.mark.parametrize("ticked, number_desired, called_with", [
    (True, True, False),
    (False, False, False),
    (True, False, optolith2pdf.pos.hero.sheet.attr_as_numbers[True]),
    (False, True, optolith2pdf.pos.hero.sheet.attr_as_numbers[False]),
])
def test_select_attr_as_numbers(ticked, number_desired, called_with):
    with \
            mock.patch(
                'optolith2pdf.locate_center_on_screen',
                side_effect=[ticked, not ticked],
            ), mock.patch(
                'optolith2pdf.click',
            ) as click_mock:
        optolith2pdf.select_attr_as_numbers(number_desired)

        if called_with:
            click_mock.assert_called_once_with(called_with, confidence=.99)
        else:
            assert not click_mock.called
