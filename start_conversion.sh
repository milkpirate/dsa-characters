#!/bin/bash

readonly XFB_SCREEN="1024x768x24"
readonly LANGUAGE="${LANGUAGE:-de}"  # de en nl fr sys
LOG_LEVEL=INFO

init() {
  if is_debug; then
    set -o xtrace  # trace commands
    LOG_LEVEL=DEBUG
  fi

  set -o errexit  # exit on errors
  set -o pipefail # errors in pipe get propagated
  # set -o nounset  # error on using empty vars

  IFS=$'\n\t' # enable strict mode
}

main () {
  local chars
  local -r language="${2:-$LANGUAGE}"

  init_x_env &
  sleep 2

  is_debug && start_vnc_server &
  opto &

  chars="$(get_char_files characters/)"

  for char_file in $chars; do
    ./optolith2pdf.py \
      --file "$char_file" \
      --images "./recognition/images" \
      --language "$language" \
      --log_level="$LOG_LEVEL" \
      --numbers
  done
}

start_vnc_server() {
  x11vnc \
    -shared \
    -forever \
    -display :0 \
    >> /tmp/x11vnc.log \
    2>&1 &
  sleep 2
  /opt/easy-novnc \
    --addr :6080 \
    --port 5900 \
    --host localhost \
    --no-url-password
}

init_x_env() {
  Xvfb :0 -screen 0 "$XFB_SCREEN" >> ~/xvfb.log 2>&1
}

get_char_files() {
  find "$1" -name '*.json' ! -name '*_deprecated.json'
}

opto() {
  /optolith/Optolith --no-sandbox
}

is_debug() {
  [[ -n "$DEBUG" || -n "$TRACE" || -n "$CI_DEBUG_TRACE" ]]
}

case "$1" in
  "--source-only")
    true
  ;;
  "--debug")
    set -x
    export DEBUG=true
    init_x_env &
    sleep 2
    start_vnc_server &
    opto &
  ;;
  *)
    init "$@"
    main "$@"
  ;;
esac
