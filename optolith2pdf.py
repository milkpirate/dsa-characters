#!/usr/bin/env python

"""
Description

© 2018 Paul Schroeder
"""

import docopt
import numpy
import logging
import pathlib
import pyautogui
import pyscreeze
import structlog
import sys
import timeout_decorator

from box import Box

__author__ = 'Paul Schroeder'
__version__ = '0.1.0'
__project__ = 'optolith2pdf'
__release__ = f'{__version__}.0rc'
__file_path__ = __file__
__file_name__ = pathlib.Path(__file_path__).name

supported_languages = ['sys', 'de', 'en', 'nl', 'fr']

__doc__ = f"""
optolith2pdf

This is a controller for Optolith, which loads character JSON files and
exports them as PFDs.

Command line description::

    Usage:
      {__file_name__} -f <file> -i <image_folder> [ -n ] [ -l <language> ] [ -L <log_level> ]

    Options:
      -i --images <image_folder>                Where the images for recognition are stored.
      -l --language <language>                  Language of sheet ({', '.join(supported_languages)})
                                                  [default: de].
      -n --numbers                              Show attribute values (not their abbreviations).
      -f <file> --file=<file>                   Which JSON file to open [default: /dev/stdin].
      -L <log_level> --log_level=<log_level>    Log level to use ({', '.join(logging._nameToLevel)})
                                                  [default: WARNING].
      -h -? --help                              print this screen.
      --version                                 print the version of this script.

    {__project__}© 2021 by {__author__} is licensed under CC BY-NC 4.0
"""

pyautogui.PAUSE = .1
pyautogui.FAILSAFE = True

pos = Box(
    file_dialog="file_dialog.png",
    settings=Box(
        open="settings.png",
        done="done.png",
        close="close.png",
        lang=Box(
            drop_down="lang_drop_down.png",
            tick="tick.png",
            default="german.png",
            de="german.png",
            en="english.png",
            nl="niederlands.png",
            fr="french",
        ),
    ),
    hero=Box(
        load="load_hero.png",
        open="open_hero.png",
        close="exit_hero.png",
        user_folder="user_folder.png",
        wrong_version="wrong_hero_file_version.png",
        delete=Box(
            do="delete_hero.png",
            ok="delete_yes.png",
        ),
        sheet=Box(
            attr_as_numbers={
                True: "attrib_num_enabled.png",
                False: "attrib_num_disabled.png",
            },
            open_pakt="open_sheet_pakt.png",
            open="open_sheet.png",
            save_as_pdf="save_as_pdf.png",
            address_bar="address_bar.png",
            confirm_override="override_yes.png",
            saved_ok="saved_ok.png",
        )
    ),
    incompatability=Box(
        message="incompatibility_msg.png",
        ok="incompatibility_ok.png",
    ),
)

logger = None
images = None


def main(argv=sys.argv):
    """
    Main function to be called on entrance.

    :param argv: Argument list (usually ``sys.argv[1:]``).
    """
    global logger, images

    args = docopt.docopt(
        doc=__doc__,
        argv=argv,
        help=True,
        version=f"{__project__} {__version__}",
    )

    log_level = args['--log_level']
    lang = args['--language']
    numbers = args['--numbers']
    images = pathlib.Path(args['--images']).resolve()
    file = pathlib.Path(args['--file']).resolve()

    logger = get_logger(log_level)
    logger.debug("Arguments parsed", args=args)

    change_language(lang_abrv=lang)
    import_hero(file=file)
    ok_incompatibility(file)

    target_file = file.with_suffix('.pdf')
    export_pdf(target_file=target_file, attr_as_numbers=numbers)

    logger.debug("Closing hero.")
    click(pos.hero.close, confidence=.99)
    delete_hero()


def get_logger(log_level):
    log_level = getattr(logging, log_level)

    structlog.configure(
        processors=[
            structlog.processors.add_log_level,
            structlog.processors.StackInfoRenderer(),
            structlog.processors.UnicodeDecoder(),
            structlog.processors.CallsiteParameterAdder(
                parameters=[
                    # structlog.processors.CallsiteParameter.FILENAME,
                    structlog.processors.CallsiteParameter.FUNC_NAME,
                    structlog.processors.CallsiteParameter.LINENO,
                ] if log_level <= logging.DEBUG else []
            ),
            structlog.dev.set_exc_info,
            # structlog.processors.TimeStamper(),
            structlog.dev.ConsoleRenderer(),
        ],
        wrapper_class=structlog.make_filtering_bound_logger(log_level),
        context_class=dict,
        logger_factory=structlog.PrintLoggerFactory(),
        cache_logger_on_first_use=True,
    )

    new_logger = structlog.getLogger(__name__)
    return new_logger


def export_pdf(target_file, attr_as_numbers=False):
    logger.info("Exporting hero to file", file=str(target_file))

    click(pos.hero.open)

    wait_for_location(pos.hero.close)   # wait till th UI has loaded
    # there are two tab bars one with "pakt" and one without
    sheet_open_loc = locate_center_on_screen(pos.hero.sheet.open_pakt)
    sheet_open_loc = locate_center_on_screen(pos.hero.sheet.open) if not sheet_open_loc else sheet_open_loc
    click(sheet_open_loc)

    wait_for_location(pos.hero.sheet.save_as_pdf, confidence=.9)
    logger.info("Attributes as numbers", numbers=attr_as_numbers)
    select_attr_as_numbers(attr_as_numbers)

    logger.debug("Entering save file dialog")
    click(pos.hero.sheet.save_as_pdf, confidence=.9)
    wait_for_location(pos.file_dialog, confidence=.95)

    pyautogui.hotkey('ctrl', 'a')
    logger.debug("Enter file path", path=str(target_file))
    pyautogui.write(f'{target_file}\n', interval=0.1)    # two enterns, for whatever reason

    pyautogui.sleep(1)

    click(pos.hero.sheet.confirm_override, confidence=.70, must_be_found=False)
    click(pos.hero.sheet.saved_ok, confidence=.99)


def select_attr_as_numbers(nums):
    on_pos = locate_center_on_screen(pos.hero.sheet.attr_as_numbers[True], confidence=.99)
    off_pos = locate_center_on_screen(pos.hero.sheet.attr_as_numbers[False], confidence=.99)

    needs_turn_on = nums and off_pos
    needs_turn_off = not nums and on_pos

    logger.debug("Current positions", off_pos=off_pos, on_pos=on_pos)
    logger.info("Numbers ticked", numbers_ticked=bool(on_pos))

    if not needs_turn_on and not needs_turn_off:
        logger.info("Attributes as numbers already as desired", desired=nums)
        return

    logger.info("Selecting attributes as numbers as desired", desired=nums)

    if needs_turn_on:
        click(pos.hero.sheet.attr_as_numbers[False], confidence=.99)
    if needs_turn_off:
        click(pos.hero.sheet.attr_as_numbers[True], confidence=.99)


def ok_incompatibility(file):
    logger.info("Check for incompatibility...")
    incomp_ok = locate_center_on_screen(pos.incompatability.ok, confidence=.95)
    if incomp_ok:
        logger.error("Hero file was created/edited with a newer version of Optolith!", file=file)
        pyautogui.screenshot(pos.hero.wrong_version)
        click(incomp_ok)
        raise ValueError(f'Hero file {file} was created/edited with a newer version of Optolith!')


def find_max_conf(image):
    conf = 1
    while conf and not locate_center_on_screen(image, confidence=conf):
        conf -= .01
    return conf


def delete_hero():
    logger.info("Remove heros from Optolith")
    wait_for_location(pos.hero.delete.do, confidence=.9)

    while delete_do := locate_center_on_screen(pos.hero.delete.do, confidence=.9):
        logger.debug("Still some heros left, removing them...")
        click(delete_do)
        click(pos.hero.delete.ok)
        pyautogui.sleep(.1)


def wait_for_location(image, *args, timeout=60, **kwargs):
    @timeout_decorator.timeout(timeout)
    def locate():
        while not (location := locate_center_on_screen(image, *args, **kwargs)):
            logger.debug(f'Searching...', image=image)
            pyautogui.sleep(.25)

        return location

    try:
        return locate()
    except timeout_decorator.timeout_decorator.TimeoutError:
        logger.error(
            f"Could not find image in time!",
            timeout=timeout,
            image=image
        )
        return None


def click(
    x_img,
    y_conf=None,
    confidence=.95,
    clicks=1,
    must_be_found=True,
    grayscale=False,
):
    click.counter += 1
    counter = click.counter

    if get_log_level() <= logging.DEBUG:
        pyautogui.screenshot(f"pre_click_{counter:02d}.png")

    if isinstance(x_img, str):
        logger.debug("Image received", received=x_img)
        confidence = y_conf if y_conf else confidence

        if must_be_found:
            return pyautogui.click(wait_for_location(
                x_img,
                confidence=confidence,
                grayscale=grayscale,
            ))

        if box_center := locate_center_on_screen(
            x_img,
            confidence=confidence,
            grayscale=grayscale,
        ):
            return pyautogui.click(box_center)

        return

    if isinstance(x_img, (int, float, numpy.int64)):
        logger.debug("Coordinates received", received=(int(x_img), int(y_conf)))
        return pyautogui.click(x_img, y_conf, clicks=clicks)

    if isinstance(x_img, (pyautogui.Point, pyscreeze.Point)):
        point = pyautogui.Point._make(map(int, x_img))
        logger.debug("Point received", received=point)
        return pyautogui.click(*point, clicks=clicks)

    logger.error("Received an unknown!", received=x_img, type=type(x_img))
    raise ValueError(f"Received an unknown: {x_img} of type {type(x_img)}")


click.counter = 0


def resolve_image(image):
    return str(images / image)


def import_hero(file):
    logger.info("Open hero", file=str(file))
    click(pos.hero.load, confidence=.8)
    wait_for_location(pos.file_dialog, confidence=.95)
    pyautogui.write(f'{file}\n', interval=0.1)


def locate_all_center_on_screen(image, confidence=.95):
    image = resolve_image(image)

    boxes = pyautogui.locateAllOnScreen(image, confidence=confidence)
    boxes = [pyscreeze.Box._make(map(int, b)) for b in boxes]
    logger.debug(f"Found boxes for image", image=image, boxed=boxes)

    centers = [pyautogui.center(b) for b in boxes]
    centers = [pyautogui.Point._make(map(int, c)) for c in centers]
    logger.debug(f"Found centers for image", image=image, centers=centers)

    return centers


def locate_center_on_screen(image, *args, confidence=.99, **kwargs):
    image = resolve_image(image)
    try:
        return pyautogui.locateCenterOnScreen(
            image,
            *args,
            confidence=confidence,
            **kwargs
        )
    except pyautogui.ImageNotFoundException as e:
        logger.warn(f"Could not find image", image=image, exception=str(e))
        return None


def change_language(lang_abrv='en'):
    logger.info("Set language", lang=lang_abrv)
    click(pos.settings.open)

    lang_image = pos.settings.lang.get(lang_abrv, "default")
    logger.debug(f"Using language image", language=lang_abrv, image=lang_image)

    wait_for_location(pos.settings.lang.drop_down)

    for drop_down in locate_all_center_on_screen(pos.settings.lang.drop_down):
        logger.debug(f"Found drop-down menu", location=drop_down)
        click(drop_down)

        logger.debug(f"Looking for language position", language=lang_image)
        lang_pos = wait_for_location(lang_image, confidence=.99, timeout=1)

        if not lang_pos:
            logger.warn(f"Could not find language location!", language=lang_image)
            continue

        logger.debug(f"Found location for language", language=lang_abrv, location=lang_pos)
        click(lang_pos)
        break

    logger.debug(f"Done")
    click(pos.settings.done, confidence=.99)


def get_log_level():
    bound_logger = structlog._config._CONFIG.default_wrapper_class.__name__
    log_level_name = bound_logger.replace('BoundLoggerFilteringAt', '').upper()
    log_level_int = getattr(logging, log_level_name)
    return log_level_int


if __name__ == "__main__":  # pragma: no cover
    main(argv=sys.argv[1:])
