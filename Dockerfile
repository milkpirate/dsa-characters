FROM ghcr.io/astral-sh/uv:python3.13-bookworm-slim AS python

ARG OPTOLITH_VERSION=1.5.1
ARG EASY_NOVNC_VERSION=1.1.0
ARG DEBIAN_FRONTEND=noninteractive

# de en fr nl
ENV LANGAUGE=de \
  DISPLAY=:0.0 \
  INPUT_FOLDER=.

# install libs (mostly for chrome)
# DL3008: Pin versions in apt get install
# DL3009: Delete the apt-get lists after installing something
# hadolint ignore=DL3008,DL3009
RUN apt-get update \
  && apt-get install --yes --no-install-recommends \
    libasound2 \
    libdbus-1-3 \
    libgbm-dev \
    libgdk-pixbuf2.0-0 \
    libglib2.0-0 \
    libgtk-3-0 \
    libnss3 \
    libx11-6 \
    libx11-xcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxss1 \
    libxtst6

# other tools
# hadolint ignore=DL3008
RUN apt-get install --yes --no-install-recommends \
    gnome-screenshot \
    nano \
    psmisc \
    x11vnc \
    xauth \
    xdotool \
    xvfb \
  && rm -rf /var/lib/apt/lists/*

ADD --link --chmod=0755 https://github.com/pgaskin/easy-novnc/releases/download/v${EASY_NOVNC_VERSION}/easy-novnc_linux-64bit /opt/easy-novnc
ADD --link optolith/*-Optolith_${OPTOLITH_VERSION}.tar.gz /
COPY pyproject.toml uv.lock /

RUN mkdir -vp /Xauthority \
  && touch /root/.Xauthority \
  && mv Optolith_* /optolith \
  && echo "Using Optolith version: $OPTOLITH_VERSION" \
  && uv pip install \
    --system \
    --no-sources \
    --link-mode=symlink \
    --compile-bytecode \
    --requirement=pyproject.toml \
  && rm -vr uv.lock pyproject.toml

# extension fix, should be superfluous when 1.5.2 is released
RUN mkdir -pv /root/.config/Optolith
ADD optolith/dev-extensions-fix.tgz /root/.config/Optolith

EXPOSE 5900/tcp
EXPOSE 6080/tcp

ENTRYPOINT ["bash", "-c"]
CMD ["exec bash"]
