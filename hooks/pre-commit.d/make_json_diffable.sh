#!/bin/bash

find . -type f -name '*.json' | while read -r file; do
  echo "Prettifying $file..."
  jq -S . "$file" | sponge "$file"
done
