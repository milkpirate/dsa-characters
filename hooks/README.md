# Git hooks
To enable hooks included in this repo for this repo here, run
```bash
git config core.hooksPath hooks
```
in the repo root.

# ATTENTION
You should **never(!)** run code from a foreign repo without reviewing it!

# References
- [man githooks](https://git-scm.com/docs/githooks)
- [man core.hooksPath](https://git-scm.com/docs/git-config#Documentation/git-config.txt-corehooksPath)
